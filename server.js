var app = require('http').createServer();
var io = require('socket.io')(app);

app.listen(3001);

io.on('connection', function (socket) {
  console.log('connected');
  socket.send('hello there')
  socket.emit('news', { hello: 'world' });
  socket.on('message', function (data) {
    console.log(data);
  });
});
